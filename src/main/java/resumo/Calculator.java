package resumo;

import java.util.Optional;
import java.util.Scanner;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Calculator {


  public static double retornaResultadoOperacao(String operand1, String operand2, String sinal) {

    double result = 0;

    if(operand1 != null && operand2 != null && sinal != null) {
      result = Operator.doSinal(sinal)
          .apply(Double.valueOf(operand1), Double.valueOf(operand2));
    }
    return result;

  }

  public static void main(String[] args) {
    System.out.println("Por favor, insira a expressão matemática: Ex: 1 + 2 ");
    // Scanner
    System.out.println(Calculator.retornaResultadoOperacao("6", "3", "+"));
  }

}