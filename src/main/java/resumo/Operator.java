package resumo;

import java.util.function.BiFunction;
import java.util.stream.Stream;
import lombok.Data;


public enum Operator {
  ADD("+", (a, b) -> a + b),
  SUB("-",(a, b) -> a - b),
  MUL("*",(a, b) -> a * b),
  DIV("/",(a, b) -> a / b);

  private final BiFunction<Double, Double, Double> funcao;
  private final String sinal;

  Operator(String sinal, BiFunction<Double, Double, Double> funcao) {
    this.funcao = funcao;
    this.sinal = sinal;
  }

  public Double apply(Double a, Double b) {
    return funcao.apply(a, b);
  }


  public static Operator doSinal(String sinal) {
    Operator[] values = Operator.values();
    for(Operator v: values) {
      if(v.sinal.equals(sinal)) {
        return v;
      }
    }
    return null;
  }
}
