package threads;

import java.util.Random;
import java.util.stream.IntStream;

public class PiGenerator {

  public double gerarPi(int numPoints) {
    int numInside = 0; // número de pontos dentro do círculo

    Random rand = new Random();

    for (int i = 0; i < numPoints; i++) {
      // gera um ponto aleatório dentro do quadrado unitário
      double x = rand.nextDouble();
      double y = rand.nextDouble();

      // verifica se o ponto está dentro do círculo unitário
      if (x*x + y*y <= 1) {
        numInside++;
      }
    }

    // a área do círculo unitário é Pi/4, então podemos estimar Pi como 4*(número de pontos dentro do círculo)/(número total de pontos)
    double piEstimate = 4.0 * numInside / numPoints;

    return piEstimate;
  }
















  public double gerarPiParalelo(int numPoints) {
    Random rand = new Random();

    int[] numInside = {0}; // número de pontos dentro do círculo

    IntStream.range(0, numPoints)
        .parallel()
        .forEach(i -> {
          // gera um ponto aleatório dentro do quadrado unitário
          double x = rand.nextDouble();
          double y = rand.nextDouble();

          // verifica se o ponto está dentro do círculo unitário
          if (x*x + y*y <= 1) {
            synchronized (numInside) {
              numInside[0]++;
            }
          }
        });

    // a área do círculo unitário é Pi/4, então podemos estimar Pi como 4*(número de pontos dentro do círculo)/(número total de pontos)
    double piEstimate = 4.0 * numInside[0] / numPoints;

    return piEstimate;
  }

}
