package solid.lsp;

public class Agua implements Cafeteira {

  @Override
  public void servirCafe() {
    // nao faz nada
  }

  public static void main(String[] args) {
    Agua agua = new Agua();
    agua.servirCafe();
  }
}
