package solid.lsp;

public interface Veiculo {

  void correr();
  void andarNaAgua();

}
