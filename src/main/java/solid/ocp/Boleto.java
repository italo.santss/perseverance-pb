package solid.ocp;

public class Boleto implements Pagamento {

  private String nome;
  private String codigoBarras;

  @Override
  public void validaPagamento() {
    // validacao de boleto
    // diferente da validacao do cartao
  }

  @Override
  public void processaPagamento() {
    System.out.println("Pagou no Boleto");
  }
}
