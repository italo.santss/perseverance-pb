package solid.ocp;

public interface Pagamento {

  void validaPagamento();
  void processaPagamento();

}
