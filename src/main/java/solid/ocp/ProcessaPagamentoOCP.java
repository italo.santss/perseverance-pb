package solid.ocp;

public class ProcessaPagamentoOCP {

  public void pagar(Pagamento pagamento) {
    pagamento.validaPagamento();
    pagamento.processaPagamento();
  }

  public static void main(String[] args) {
    ProcessaPagamentoOCP processaPagamento = new ProcessaPagamentoOCP();

    processaPagamento.pagar(new Boleto());
  }

}
