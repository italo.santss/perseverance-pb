package solid.ocp;
import java.util.Comparator;
import java.util.List;

public class App {
  public static void main(String[] args) throws Exception {
    System.out.println("Hello, World!");

    Integer[] array = { 1, 4, 3, 2, 10, 21, 17, 100 };
    Comparator<Integer> comparator = (x, y) -> Integer.compare(x, y);
    List<Integer> list = MyOwnList.asListedSorted(array, comparator);

    System.out.println(list.toString());

  }
}
