package solid.ocp;

import lombok.Data;

@Data
public class CartaoCredito implements Pagamento {
  private int cvv;
  private String nome;
  private String numero;

  @Override
  public void validaPagamento() {
    // validacao do cartao diferente de boleto
  }

  @Override
  public void processaPagamento() {
    System.out.println("Pagamento no Cartao de Credito");
  }
}

