package solid.ocp;


import java.util.Arrays;
    import java.util.Collections;
    import java.util.Comparator;
    import java.util.List;

public class MyOwnList {

  public static <T> List<T> asListedSorted(T[] array, Comparator<T> comparator) {
    List<T> list = Arrays.asList(array);
    Collections.sort(list, comparator);

    return list;
  }
}

