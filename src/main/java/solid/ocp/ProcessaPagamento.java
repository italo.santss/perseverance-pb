package solid.ocp;


public class ProcessaPagamento {

  public <T> void pagar(T pagamento) {

    if(pagamento instanceof CartaoCredito) {
      /**
       * Efetua validacoes do Cartao de Credito
       * nome
       * cvv
       * data de expiracao
       * numero
       */
      System.out.println("Pagou no Credito");
    }

    if(pagamento instanceof CartaoDebito) {
      /**
       * Efetua validacoes do Cartao de Credito
       * nome
       * cvv
       * data de expiracao
       * numero
       */
      System.out.println("Pagou no Débito");
    }
    if(pagamento instanceof Boleto) {
      // validacoes
      // verificar a data do boleto

      System.out.println("Pagou no Boleto");
    }
  }

  public static void main(String[] args) {
    ProcessaPagamento processaPagamento = new ProcessaPagamento();

    processaPagamento.pagar(new CartaoDebito());
  }


}