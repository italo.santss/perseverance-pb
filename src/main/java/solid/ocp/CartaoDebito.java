package solid.ocp;

import lombok.Data;

@Data
public class CartaoDebito implements Pagamento {
  private int cvv;
  private String nome;

  @Override
  public void validaPagamento() {
    // validacao do cartao diferente de boleto e de credito
  }

  @Override
  public void processaPagamento() {
    System.out.println("Pagamento no Cartao de Debito");
  }
}