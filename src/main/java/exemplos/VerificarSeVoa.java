package exemplos;

public class VerificarSeVoa implements Verificador{

  @Override
  public boolean verficar(Animal animal) {
    return animal.isPodeVoar();
  }
}
