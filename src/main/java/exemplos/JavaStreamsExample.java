package exemplos;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class JavaStreamsExample {



  /**
   * Continuacao Programacao Funcional - Streams e Lambda
   * <p>
   * - classe Function - classe Supplier
   * <p>
   * - Introducao a Streams - .map()
   * <p>
   * <p>
   * intermediarias finais
   */
  public static void main(String[] args) {

    List<Pessoa> listPessoa = Arrays.asList(
        new Pessoa("Amanda", "BancoBrasil", 25),
        new Pessoa("Bruno", "BancoBrasil", 22),
        new Pessoa("Alex", "BancoBrasil", 22),
        new Pessoa("Hugo", "BancoBrasil", 22),
        new Pessoa("Italo", "BancoBrasil", 18),
        new Pessoa("Lucas", "BancoBrasil", 17),
        new Pessoa("Natan", "BancoBrasil", 26)
    );

    // Uma lista de pessoas com idade.
    // Filtrar apenas maiores de 20 anos
    // Converter essa lista de (Pessoa) para o
    // (PessoaDTO) com o nome+setor preenchido

    /**
     * Desafio do dia
     * Vamos tentar usar:
     *  - filter()
     *  - map()
     *  - collect()
     *  - Collectors.groupingBy()
     *
     *  Agrupar lista de transacoes
     *  por categoria e filtrar valores acima de 100
     *
     *  Bonus(Valores acima de 100, categorizar como importante)
     *
     *  new Transacoes("Compras no mercado", 50.0, "Alimentacao"),
     *  new Transacoes("Compras em  restaurante", 150.0, "Alimentacao"),
     *  new Transacoes("Roupas", 75.0, "Vestuario"),
*         new Transacoes("Combustivel", 25.0, "Transporte"),
*         new Transacoes("Entretenimento", 140.0, "Lazer"),
     */

    Map<Integer, List<PessoaDto>> groupporidade = listPessoa.stream()
        .filter(pessoa -> pessoa.getIdade() > 20)
        // manipulando Pessoa daqui pra cima
        .map(JavaStreamsExample::toDto)
        // manipulando Pessoa DTO
        .collect(Collectors.groupingBy(PessoaDto::getIdade));

    //pessoaDto.forEach(System.out::println);

//    List<Transacoes> transactions = Arrays.asList(
//        new Transacoes("Compras no mercado", 50.0, "Alimentacao"),
//        new Transacoes("Roupas", 75.0, "Vestuario"),
//        new Transacoes("Combustivel", 25.0, "Transporte"),
//        new Transacoes("Entretenimento", 140.0, "Lazer"),
//        new Transacoes("Cinema", 20.0, "Lazer"),
//        new Transacoes("Salario", 3000.0, "Renda")
//    );
//
//    Map<String, List<String>> transacoesPorListaString = transactions
//        .stream()
//        .collect(
//            Collectors.groupingBy(
//                t -> t.getValor() >= 100 ? "Importante" : t.getCategoria(),
//                Collectors.mapping(Transacoes::getDescricao, Collectors.toList()))
//        );
//
//    Map<String, String> transacoesPorString = transactions
//        .stream()
//        .collect(
//            Collectors.groupingBy(
//                t -> t.getValor() >= 100 ? "Importante" : t.getCategoria(),
//                Collectors.mapping(Transacoes::getDescricao, Collectors.joining(" - "))));
//
//    System.out.println(transacoesPorString);

//    Map<String, List<String>> transactionsByCategory = transactions
//        .stream()
//        .collect(
//            Collectors.groupingBy(
//                Transacoes::getCategoria,
//                Collectors.mapping(Transacoes::getDescricao, Collectors.toList()))
//        );



  }
  // solucao do Bruno
  public static PessoaDto toDto(Pessoa pessoa) {
    return PessoaDto.builder()
        .nome(pessoa.getNome())
        .idade(pessoa.getIdade())
        .nomeEsetor(pessoa.getNome()
            .concat(pessoa.getSetor()))
        .build();
  }


}
