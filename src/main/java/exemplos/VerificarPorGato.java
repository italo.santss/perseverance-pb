package exemplos;

public class VerificarPorGato implements Verificador{

  @Override
  public boolean verficar(Animal animal) {
    return animal.getEspecie().equals("Gato");
  }
}
