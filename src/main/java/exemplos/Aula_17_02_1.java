package exemplos;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

public class Aula_17_02_1 {

  public static void main(String[] args) throws InterruptedException {
    // thenCompose
    // thenCombine
    long start = System.currentTimeMillis();
    // 1 - Buscar pessoa pelo nome
    // 2 - Buscar o setor dessa pessoa

    buscarPessoa("Italo")
        .thenApply(p -> buscarSetorFixo());

    buscarPessoa("")
        .thenCompose(p -> buscarPessoa(""));

    buscarPessoa("Hugo")
        .thenCombine(buscarSetorFixo(), (pess, set) -> "A pessoa é "+pess.getNome()+" setor: " + set)
    .join();

    System.out.println("total execucao: " + (System.currentTimeMillis() - start) + " ms");
  }

  public static CompletableFuture<Pessoa> buscarPessoa(String nome) {
    // codigo demorado antes da criacao do CF
    return CompletableFuture.supplyAsync(() -> {
      try {
        TimeUnit.SECONDS.sleep(2);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      return Pessoa.builder()
          .nome(nome)
          .idade(18)
          .build();
    });
  }

  public static CompletableFuture<String> buscarSetorFixo() {

    return CompletableFuture.supplyAsync(() -> {
      try {
        TimeUnit.SECONDS.sleep(1);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      return "O setor é financeiro";
    });
  }

  public static String buscarSetorPessoa(Pessoa pessoa) throws InterruptedException {
    TimeUnit.SECONDS.sleep(1);
    return "O setor de " + pessoa.getNome() + " é financeiro";
  }

}
