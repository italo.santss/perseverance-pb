package exemplos;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import threads.PiGenerator;

/**
 * Introducao a Programacao Assincrona
 * Exercicio de Files e File
 */
public class Aula_13_02 {

  public static void main(String[] args) throws InterruptedException, ExecutionException {
      // < Java 8
      // >= Java 8

    long start = System.currentTimeMillis();
    PiGenerator piGenerator = new PiGenerator();

   CompletableFuture<Double> future1 = CompletableFuture
       .supplyAsync(() -> piGenerator.gerarPi(10000000)
   );
    CompletableFuture<Double> future2 = CompletableFuture
        .supplyAsync(() -> piGenerator.gerarPi(10000000)
        );

    future1
        .thenAccept(res -> CompletableFuture
            .supplyAsync(() -> piGenerator.gerarPi(10000000)));


    future1.thenCompose(res ->
        CompletableFuture.supplyAsync(() -> recebeNumeroPi(res)));

    System.out.println("ola mundo");
    // combinar varios metodos



//  CompletableFuture<Double> future2 = CompletableFuture
//      .supplyAsync(() -> piGenerator.gerarPi(10000000)
//      );

    future1
        .thenCompose(pi ->
            CompletableFuture.supplyAsync(() -> recebeNumeroPi(pi)));

    CompletableFuture<String> resultadoFinal = future2
        .thenCombine(future1, (fut2, fut1) -> "Resultado combinado " + fut1 + " " + fut2);

    // blocante
    // metodo final
    System.out.println(resultadoFinal.get());

    System.out.println("Tempo Total: ");
    System.out.println((System.currentTimeMillis() - start) + " ms");
  }

  public static String composicaoFinal(String future1, String future2) {
    return future1.concat(future1);
  }

  public static void buscaDoBancoDedados(double pi) {
    System.out.println("Total de Pi é " + pi);
  }

  public static CompletableFuture<String> buscarProdutoAsync(double pi) {
    System.out.println("Total de Pi é " + pi);
    return CompletableFuture.completedFuture("teste");
  }

  public static String recebeNumeroPi(double pi) {
    return "Total de Pi é " + pi;
  }

  public static CompletableFuture<String> fazOutraCoisa() throws InterruptedException {
    TimeUnit.SECONDS.sleep(2);
    return CompletableFuture.completedFuture("Executar outra tarefa");
  }

}
