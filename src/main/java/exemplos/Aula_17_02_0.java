package exemplos;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Aula_17_02_0 {

  // de uma lista de inteiros. Calcular a soma de forma assincrona
  public static void main(String[] args) throws ExecutionException, InterruptedException {

    System.out.println("Dentro do main: "+ Thread.currentThread().getName());
    List<Integer> listaInteiros = Arrays.asList(2, 3, 4, 6, 2, 10);

    ExecutorService executorService = Executors.newFixedThreadPool(2);

    // definir nosso CompletableFuture
    CompletableFuture<Integer> soma = CompletableFuture.supplyAsync(() ->  {
      // escrever a logica aqui
      try {
        TimeUnit.SECONDS.sleep(4);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      System.out.println("Dentro do future: " + Thread.currentThread().getName());
      return listaInteiros.stream().collect(Collectors.summingInt((n) -> n));
    }, executorService);

    CompletableFuture<String> futuro2 = CompletableFuture
        .supplyAsync(() -> {

          System.out.println("Dentro do future2: " + Thread.currentThread().getName());
          return "Outro future";
        }, executorService);



    soma.thenAccept(System.out::println);
    futuro2.thenAccept(System.out::println);

    System.out.println("Final do código");

    executorService.shutdown();

    //Integer resultado = soma.get(); // blocante
    //System.out.println(resultado);

  }


}
