package exemplos;


import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Animal {

  private String especie;
  private String nome;
  private boolean podeVoar;
  private Integer idade;

}
