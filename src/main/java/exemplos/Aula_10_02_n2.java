package exemplos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Aula_10_02_n2 {





  public static void main(String[] args) {
    File file = new File("src/main/resources/arquivo.txt");

    try {
      BufferedReader br = new BufferedReader(new FileReader(file));
      String line;
      while((line = br.readLine()) != null) {
        System.out.println(line);
      }
    } catch (FileNotFoundException e) {
      System.out.println("Erro ao ler arquivo");
    } catch (IOException e) {
      System.out.println("Erro ao ler linha");
    }

    // nio
    Path path = Paths.get("src/main/resources/arquivo.txt");
    try {
      Files.lines(path).forEach(System.out::println);
    } catch (IOException e) {
      e.printStackTrace();
    }

//    File file = new File("src/main/resources/arquivo2.txt");
//    if(file.exists()) {
//
//      System.out.println("Arquivo Existe em " + file.getAbsolutePath());
//    }else {
//
//      try {
//        file.createNewFile();
//        System.out.println("Arquivo criado com sucesso");
//      } catch (IOException e) {
//        System.out.println("Erro ao criar arquivo");
//      }
//
//      System.out.println("Arquivo Nao Existe em" + file.getAbsolutePath());
//    }
  }

}
