package exemplos;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class JavaLambdaExample {

  /**
   * Programacao Funcional
   *
   * Introducao
   * Interfaces Funcionais
   * Entendendo como funciona expressões Lambda
   * Algumas interfaces Funcionais do Java
   *  - Comparator
   *  - Predicate
   *  - Function
   *
   *
   */

  public static void main(String[] args) {
    List<Animal> animais = new ArrayList<>();

    animais.add(new Animal("Gato", "Gatonildo", false, 10));
    animais.add(new Animal("Cachorro", "Rex", false, 12));
    animais.add(new Animal("Pato", "APatolino", true, 30));
    animais.add(new Animal("Cachorro", "Luna", false, 13));
    animais.add(new Animal("Tartaruga", "Tuga", false, 230));
    animais.add(new Animal("Cachorro", "BabyDoge", false, 5));


//    animais.stream()
//        .filter()


    Predicate<Animal> testaSeEhCachorro = new Predicate<Animal>() {
      @Override
      public boolean test(Animal o) {
        return o.getEspecie().equals("Cachorro");
      }
    };


    // Ideia de exercicio:
    // Dado uma lista de pessoas, filtre todas as pessoas que
    // são adultas e ordene por
    // seu nome em ordem crescente, se o nome for igual,
    // organize por idade em
    // ordem crescente
    //


    //estrutura do lambda
    // (argumento) -> { expressao }


    List<Animal> cachorros = animais.stream()
        .filter((animal) -> animal.getEspecie().equals("Cachorro"))
        .collect(Collectors.toList());

    cachorros.forEach(System.out::println);

    Comparator ordemCrescente = new Comparator<Animal>() {
      @Override
      public int compare(Animal a1, Animal a2) {
        return a1.getNome().compareTo(a2.getNome());
      }
    };



    Comparator<Animal> ordemDecrescente =
        (a1, a2) -> a2.getEspecie().compareTo(a1.getEspecie());
    Comparator<Animal> porIdade =
        Comparator.comparingInt(Animal::getIdade);



    // lista de animais por ordem decrescente de especie
    // porem se contem especie igual, ordenar por idade
//    animais.sort(ordemDecrescente
//        .thenComparing(porIdade));


//    animais.forEach(an -> System.out.println(an.getEspecie()
//        .concat(" : " + (an.getIdade()))));







//    filtrar(animais, new VerificarSeVoa());
//    filtrar(animais, new VerificarPorGato());

//    filtrar(animais, animal ->  animal.isPodeVoar());
//    filtrar(animais, animal -> animal.getEspecie().equals("Cachorro"));
//
//    filtrar(animais, animal ->  !animal.isPodeVoar());

    // (argumento) -> {Expressao}



    //filtrar(animais, animal ->  !animal.isPodeVoar());

  }

  private static void filtrar(List<Animal> animais, Predicate<Animal> verificador) {

    for(Animal animal: animais) {
      if(verificador.test(animal)) {
        System.out.println(animal.getNome());
      }
    }
  }



}
