package exemplos;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Transacoes {
  private String descricao;
  private BigDecimal valor;
  private String categoria;
}
