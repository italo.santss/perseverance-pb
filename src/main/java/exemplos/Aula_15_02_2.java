package exemplos;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

// ler um arquivo de forma assincrona
public class Aula_15_02_2 {

  public static void main(String[] args) throws ExecutionException, InterruptedException {
    Path path = Paths.get("src/main/resources/arquivo.txt");

    CompletableFuture<String> conteudoFile = CompletableFuture
        .supplyAsync(() -> {
      try {
        return new String(Files.readAllBytes(path));
      } catch (IOException e) {
        return "";
      }
    });

    CompletableFuture<String> futuro2 = CompletableFuture
        .supplyAsync(() -> "Final do arquivo");

    // thenAccept
    // thenCompose - standBy
    // thenCombine
    // thenApply

    // Colocar conteudo para maiusculas
    CompletableFuture<String> cfMaiusculas = conteudoFile.thenApply(String::toUpperCase);
    //System.out.println(cfMaiusculas.get());
    System.out.println("Ultima linha");
  }
}
