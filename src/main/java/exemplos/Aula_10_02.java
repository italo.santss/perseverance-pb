package exemplos;


import java.util.function.BiFunction;

public class Aula_10_02 {

  /**
   * BiFunction
   * Introducao Files NIO
   * Resumao modulo
   */

  public static void main(String[] args) {

    // como podemos somar dois numeros inteiros
    // usando o BiFunction

    BiFunction<Integer, Integer, Integer> somar = (n1, n2) -> n1 + n2;
    System.out.println(somar.apply(4,2));

    // Como concatenar duas Strings
    // por exemplo str1 = Hello
    // str2 = Bye
    BiFunction<String, String, String> concatenar = (s1, s2) -> s1.concat(s2);
    System.out.println(concatenar.apply("Hello", "Mundo"));

    // Uma funcao para calcular Salario
    // baseado nas horas trabalhadas e no salario base
    // primeiro parametro eh o salario base
    // segundo parametro eh a qtd de horas
    BiFunction<Double, Integer, Double> calculaSalario = (b1, hora) -> b1 * hora;
    System.out.println(calculaSalario.apply(80.0, 160));





  }

}
