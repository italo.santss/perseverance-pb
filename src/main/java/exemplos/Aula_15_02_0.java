package exemplos;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

/**
 * REVISAO - Manipulando Arquivos usando NIO
 * REVISAO - Usando Streams e Collectors Grouping mapping
 * Continuacao de CompletableFuture
 */
public class Aula_15_02_0 {
  public static void main(String[] args) throws IOException {
    Path arquivo = Paths.get("src/main/resources/arquivo.txt");
    Path arquivoOutput = Paths.get("src/main/resources/output.txt");

    Map<Integer, List<String>> nomesPorIdade = Files.lines(arquivo)
        .map(linha -> linha.split(","))
        .collect(Collectors.groupingBy(
            campos -> Integer.parseInt(campos[1]),
            Collectors.mapping(campos -> campos[0], Collectors.toList())
        ));

    int mediaIdade = nomesPorIdade.keySet()
        .stream()
        .mapToInt(idade -> idade)
        .sum() / nomesPorIdade.size();

    String output = nomesPorIdade.entrySet().stream()
        .sorted(Entry.comparingByKey())
        .map(linha -> "Idade: " + linha.getKey() + " -> " + linha.getValue() + "")
        .collect(Collectors.joining("\n"))
        .concat("\n\nMedia de Idade: " + mediaIdade);

    System.out.println(output);
    //Files.writeString(arquivoOutput, output, StandardOpenOption.APPEND);
    Files.write(arquivoOutput, output.getBytes() ,StandardOpenOption.TRUNCATE_EXISTING);

  }
}
