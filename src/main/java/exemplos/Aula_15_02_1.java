package exemplos;


import java.time.Instant;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

// CompletableFuture
public class Aula_15_02_1 {

  public static void main(String[] args) throws ExecutionException, InterruptedException {
    long start = System.currentTimeMillis();

    ExecutorService executor = Executors.newFixedThreadPool(2);

    CompletableFuture<String> futuro1 = CompletableFuture.supplyAsync(() -> {
      try {
        // fazendo algo demorado
        TimeUnit.SECONDS.sleep(2);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      System.out.println("Fut1: " + Thread.currentThread().getName());
      return "Primeiro Future";
    },executor);
    CompletableFuture<String> futuro2 = CompletableFuture.supplyAsync(() -> {
      try {
        // fazendo algo menos demorado
        TimeUnit.SECONDS.sleep(1);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      System.out.println("Fut2: " +Thread.currentThread().getName());
      return "Segundo Future";
    },executor);


    System.out.println("Executado antes do future com a Thread -> " + Thread.currentThread().getName());
    CompletableFuture<Void> ambosFuturos = CompletableFuture.allOf(futuro1, futuro2);

    executor.shutdown();
    //ambosFuturos.join();

//    System.out.println(futuro1.get());
//    System.out.println(futuro2.get());

    System.out.println("Ultima linha do método " + (System.currentTimeMillis() - start ) + " ms");
  }
  // Ler um arquivo de forma assincrona

}
