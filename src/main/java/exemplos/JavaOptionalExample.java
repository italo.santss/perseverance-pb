package exemplos;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class JavaOptionalExample {

  /**
   * Plano 08.02
   *  - Correção exercício aula passada x
   *  - Optional
   *  - findAny() e findFirst()
   *  - anyMatch()
   */

  public static void main(String[] args) {
    // agrupar por pares e impares
//    List<Integer> lista = Arrays.asList(1, 2, 3, 4, 5, 6);
//    Map<String, List<Integer>> paresEimpares = lista.stream()
//        .collect(Collectors.groupingBy(n -> n % 2 == 0 ? "par" : "impar"));
//
//    // soma dos pares e a soma dos impares
//    Map<String, Integer> somaParesImpar = lista.stream()
//        .collect(Collectors.groupingBy(n -> n % 2 == 0 ? "par" : "impar",
//            Collectors.summingInt(i -> i)));




    List<Pessoa> listaPessoaNula = null;

//    Map<String, String> pessoasPorSetor = listaPessoa.stream()
//        .collect(Collectors.groupingBy(Pessoa::getSetor,
//            Collectors.mapping(Pessoa::getNome, Collectors.joining(", "))));

//    String nao_encontrou_ninguem = listaPessoa.stream()
//        .filter(p -> p.getIdade() < 10)
//        .map(Pessoa::getNome)
//        .findFirst().orElse("Nao encontrou ninguem");

    List<Pessoa> listaPessoa = Arrays.asList(
        null,
        new Pessoa(null, "ti", null),
        new Pessoa("Natan", "rh", 20),
        new Pessoa("Amanda", "ti", 20)
    );

    List<Integer> listaDepessoas = listaPessoa.stream()
        .map(p -> retornarIdadePessoaSomada(p))
        .collect(Collectors.toList());

    //System.out.println(listaDepessoas);

    List<Integer> lista = Arrays.asList(1, 2, 3, 4, 5, 6);
    List<Integer> pares = Arrays.asList(2,  4,  6);
    boolean temNumeroPar = lista.stream().anyMatch(n -> n % 2 == 0);
    lista.stream()
        .filter(n -> n % 2 == 0)
        .findAny();

    String teste = null;

    Optional.ofNullable(teste).ifPresentOrElse(
        valor -> System.out.println("o valor é " + valor),
        () -> System.out.println("o valor esta vazio")

    );

    int[] nums = {2,9,3,5,6};
    int[] sum = findSum(nums, 7);
    System.out.println(sum[0] + " " + sum[1]);

  }

  public static int[] findSum(int[] numbers, int targetValue) {

    HashMap<Integer, Integer> map = new HashMap<>();

    for(int i = 0; i < numbers.length; i++) {
      int temporary = targetValue - numbers[i];

      if(map.containsKey(temporary)) {
        return new int[]{map.get(temporary), i};
      }

      // include in map numbers
      map.put(numbers[i], i);
      // return
    }

    return new int[]{-1,-1};

  }

  public static Integer retornarIdadePessoaSomada(Pessoa pessoa) {
    // versao Optional
    return Optional.ofNullable(pessoa)
        .map(Pessoa::getIdade)
        .map(idade -> idade + 100)
    .orElse(0);




//    // versao antiga
//    if(pessoa != null && null != pessoa.getIdade()) {
//      return pessoa.getIdade() + 100;
//    }else {
//       return 0;
//    }
  }



/**
 * Exercicio pro final da aula
 * Dada uma lista de transacoes...
 * Criar um método para buscar as transacoes por sua
 * categoria e caso nao encontre nada, retornar uma lista vazia.
 *
 * Esse metodo deve ter protecoes de NullPointer
 *
 *
 */






}
