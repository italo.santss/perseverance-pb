package date;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class ExamploLocalDate {

  public static void main(String[] args) {



    LocalDate hoje = LocalDate.now();
    LocalDate semanaQueVem = LocalDate.now().plusYears(2).plusWeeks(1).plusDays(2);

    System.out.println(ChronoUnit.DAYS.between(hoje, semanaQueVem));

    Period diferenca = Period.between(hoje, semanaQueVem);

    System.out.println(diferenca);
    System.out.println(LocalDate.parse("2023-04-04"));





  }
}
